/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Modelo.SyscallModel;
import java.io.IOException;

/**
 *
 * @author josga
 */
public class SyscallView {

    public static void main(String[] args) throws IOException, InterruptedException {
        String message = "Usando SysCall / SO\n\n";
        String outputPath = "src/datos/salida.txt";
        SyscallModel syscall = new SyscallModel(message, outputPath);
        syscall.execute();
    }

}
