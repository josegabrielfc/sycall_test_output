/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Modelo.SyscallPing;
import java.io.IOException;

/**
 *
 * @author josga
 */
public class ShowPings {
    
    public static void main(String[] args) throws IOException, InterruptedException {
        String message = "Usando SysCall / SO\n\n";
        String outputPath = "src/datos/ping.txt";
        SyscallPing syscall = new SyscallPing(message, outputPath);
        syscall.execute(10); //Si es 0 o numero negativo, entonces solo ejecutara el resultado del comando de ping por defecto
    }
    
}
