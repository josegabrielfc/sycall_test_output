/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.io.*;

/**
 *
 * @author josga
 */
public class SyscallPing extends SyscallBase {

    private String host = "www.google.com";

    public SyscallPing(String message, String outputPath) {
        super(message, outputPath);
    }

    public String getCommandPing() {
        return "ping " + host; //"cmd.exe /c ping www.google.com"
    }

    public String getCommandNumPings(int numPings) {
        return "ping -n " + numPings + " " + host;
    }

    public void execute(int numPings) throws FileNotFoundException, IOException, InterruptedException {
        FileOutputStream output = new FileOutputStream(getOutputPath());

        System.setOut(new PrintStream(output));
        String message = getMessage();
        System.out.write(message.getBytes(), 0, message.length());

        try {
            boolean hasAccess;
            Process processPing;
            if (numPings > 0) {
                processPing = Runtime.getRuntime().exec(getCommandNumPings(numPings));
            } else {
                processPing = Runtime.getRuntime().exec(getCommandPing());
            }
            int exitValue = processPing.waitFor();
            hasAccess = (exitValue == 0);

            if (hasAccess) {
                System.out.println("La máquina tiene acceso a internet C:\n");
            } else {
                System.out.println("La máquina no tiene acceso a internet :C\n");
            }

            BufferedReader strPing = new BufferedReader(new InputStreamReader(processPing.getInputStream()));
            String line;
            String ping = "";

            while ((line = strPing.readLine()) != null) {
                ping += line + "\n";
            }
            System.out.println(ping);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        processNotepad();
        output.close();
    }
}
