/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author josga
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class SyscallModel extends SyscallBase {

    public SyscallModel(String message, String outputPath) {
        super(message, outputPath);
    }

    public String getCommandEcho() {
        return "cmd.exe /c echo %cd%";
    }

    public String getCommandDate() {
        return "cmd.exe /c date /T";
    }

    public int getCodigo() {
        return 1152085;
    }

    public void execute() throws FileNotFoundException, IOException, InterruptedException {
        
        FileOutputStream output = new FileOutputStream(getOutputPath());
        //Cambiar la salida estándar:
        System.setOut(new PrintStream(output));
        String message = getMessage();
        System.out.write(message.getBytes(), 0, message.length());

        Process process = Runtime.getRuntime().exec(getCommandEcho());
        String result1 = result(process);
        System.out.println(result1);

        int codigo = getCodigo();
        System.out.println("Código: " + codigo);

        Process dateProcess = Runtime.getRuntime().exec(getCommandDate());
        String result2 = result(dateProcess);
        System.out.println("La fecha actual es: " + result2);

        processNotepad();
        output.close();
    }
}
