/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package syscall;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

/**
 *
 * @author DOCENTE
 */
public class SysCall {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String mensaje = "Bienvenidos a su clase de SO\n";
        //Syscall a través de java
        FileOutputStream salida = new FileOutputStream("src/datos/salida.txt");
        File fileName = new File("src/datos/salida.txt");
        PrintStream inicial = System.out;
        //Cambiar la salida estándar:
        System.setOut(new PrintStream(salida));
        System.out.write(mensaje.getBytes(), 0, mensaje.length());

        //String comando1[] = {"notepad.exe", fileName.getAbsolutePath()};
        //String comando2[] = {"cmd.exe", "/c", "echo 'nada'"};
        String comando[] = {"cmd.exe", "/c", "echo %cd%"};
        String commandDate[] = {"cmd.exe", "/c", "date /T"};

        Process proceso = Runtime.getRuntime().exec(comando);

        BufferedReader is = new BufferedReader(new InputStreamReader(proceso.getInputStream()));
        String line;
        String msg = "";
        // reading the output
        while ((line = is.readLine()) != null) {
            msg += line + "\n";
        }
        System.out.println(msg);

        int codigo = 1152085;
        System.out.println("Código: " + codigo);

        //Fecha
        Process dateProcess = Runtime.getRuntime().exec(commandDate);
        BufferedReader strDate = new BufferedReader(new InputStreamReader(dateProcess.getInputStream()));
        String lineD;
        String date = "";

        while ((lineD = strDate.readLine()) != null) {
            date += lineD + "\n";
        }
        System.out.println("La fecha actual es: " + date);

        // Show NotePad :D
        Process processBlocDeNotas = Runtime.getRuntime().exec(new String[]{"notepad.exe", fileName.getAbsolutePath()});
        try {
            processBlocDeNotas.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        salida.close();
    }
}
