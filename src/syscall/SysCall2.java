/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package syscall;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jgfch
 */
public class SysCall2 {

    private Process p = null;
    private String buffer = "";
    private FileOutputStream salida = null;

    public SysCall2() {
    }

    /**
     * Ejecutar un comando
     *
     * @param command comando a ejecutar
     * @return para saber si se ejecuto bien
     */
    public String ejecutar(String[] command) {
        try {
            p = Runtime.getRuntime().exec(command);
        } catch (IOException ex) {
            System.err.print(ex.getMessage());
        }
        return "nada";
    }

    /**
     * Obtener el resultado del comando
     *
     * @return el resultado
     * @throws UnsupportedEncodingException
     */
    private String resultado() {

        if (p == null) {
            System.err.print("Error, ningun proceso se ha ejecutado y no hay resultados.");
            return "Error";
        }

        BufferedReader is = new BufferedReader(new InputStreamReader(p.getInputStream()));

        String line;

        try {
            // reading the output
            while ((line = is.readLine()) != null) {
                buffer += line + "\n";
            }
        } catch (IOException ex) {
            Logger.getLogger(SysCall.class.getName()).log(Level.SEVERE, null, ex);
        }

        return buffer;

    }

    /**
     * Crear un archivo de cualquier extension
     *
     * @param direccion donde se creara el archivo
     * @throws FileNotFoundException
     */
    public void crear(String direccion) {
        try {
            salida = new FileOutputStream(direccion);
        } catch (FileNotFoundException ex) {
            System.err.print("Error, dirección no encontrada: " + direccion);
        }
    }

    /**
     * Abre el archivo siempre y cuando sea un .txt
     *
     * @param dir direccion del archivo
     */
    public void abrir(String dir) {
        File direccion = new File(dir);
        String comando[] = {"notepad.exe", direccion.getAbsolutePath()};
        this.ejecutar(comando);
    }

    /**
     * Lee el resultado del comando
     *
     * @return el resultado almacenado en el Buffer
     */
    public String leer() {
        return this.resultado();
    }

    /**
     * Escribe en el archivo por defecto
     *
     * @param mensaje a escribir
     */
    public void escribir(String mensaje) {
        cambiarSalida();
        System.out.println(mensaje);
        reiniciarSalida();
    }

    /**
     * Escribe en el archivo especificado
     *
     * @param mensaje a escribir
     * @param direccion donde se encuentra el archivo
     */
    public void escribir(String mensaje, String direccion) {
        cambiarSalida();
        System.out.println(mensaje);
        reiniciarSalida();
    }

    /**
     * Obtener el resultado cuando se ejecuto el comando
     *
     * @return
     */
    public String getBuffer() {
        return buffer;
    }

    /**
     * Limpiar el resultado del comando ejecutado
     */
    public void limpiarBuffer() {
        this.buffer = "";
    }

    /**
     * Cambia la salida hacia el archivo que se creo antes
     */
    public void cambiarSalida() {
        System.setOut(new PrintStream(salida));
    }

    /**
     * Cambia la salida por la consola (normal)
     */
    public void reiniciarSalida() {
        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
    }

    /**
     * Cambia la salida hacia un archivo en especifico
     *
     * @param direccion donde esta el archivo
     */
    public void cambiarSalida(String direccion) {
        try {
            System.setOut(new PrintStream(new FileOutputStream(direccion)));
        } catch (FileNotFoundException ex) {
            System.err.print("Error, no se encontro el archivo en la direccion: " + direccion);
        }
    }

    /**
     * Cierra el proceso que se ejecuto antes
     *
     * @throws IOException
     */
    public void cerrarProceso() throws IOException {
        this.salida.close();
    }
}
